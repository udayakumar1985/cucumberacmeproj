import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class Learnwindowhandle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("web"
				+ "driver.chrome.driver","./driver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		driver.findElementByLinkText("Contact Us").click();
        System.out.println(driver.getTitle());
        Set<String> allwindows = driver.getWindowHandles();
        List <String> lst = new ArrayList<>();
        lst.addAll(allwindows);
        driver.switchTo().window(lst.get(0));
        System.out.println(driver.getTitle());
        
        driver.switchTo().window(lst.get(1));
        
        driver.close();

	}

}
