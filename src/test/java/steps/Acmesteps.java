package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Acmesteps {
	
	public ChromeDriver driver;
	
	@Given("Launch chrome browser")
	public void openChromeBrowser() {
	    // Write code here that turns the phrase above into concrete actions
		
           System.setProperty("webdriver.chrome.driver","./driver\\chromedriver.exe");
           
           driver = new ChromeDriver();	
		
	}

	@Given("Maximize the browser")
	public void maximizeTheBrowser() {
	    // Write code here that turns the phrase above into concrete actions
		
		  driver.manage().window().maximize();
		  driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		
	}

	@Given("Enter the Url")
	public void enterTheUrl() {
	    // Write code here that turns the phrase above into concrete actions
		 driver.get("https://acme-test.uipath.com/");
	}

	@Given("Enter the username as (.*)")
	public void enterTheUsernameAsUdayuuk22mailCom(String Uname) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("email").sendKeys(Uname);
	}

	@Given("Enter the password as (.*)")
	public void enterThePasswordAskowshika(String Upwd) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("password").sendKeys(Upwd);
	}

	@When("Click on login button")
	public void clickOnLoginButton() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("buttonLogin").click();
	}

	@Then("verify login is successful")
	public void verifyLoginIsSuccessful() {
	    // Write code here that turns the phrase above into concrete actions
		
		System.out.println("logged in successfully");
	}

	@Then("Click on Vendors")
	public void clickOnVendors() {
	    // Write code here that turns the phrase above into concrete actions
		WebElement VendorEle = driver.findElementByXPath("(//button[@class='btn btn-default btn-lg'])[4]");
	}

	@Then("Click on Search for vendors")
	public void clickOnSearchForVendors() {
	    // Write code here that turns the phrase above into concrete actions
		WebElement VendorEle = driver.findElementByXPath("(//button[@class='btn btn-default btn-lg'])[4]");
		WebElement searchVendorEle = driver.findElementByXPath("//a[contains( text (),'Search for Vendor')]");
		Actions builder = new Actions(driver);
		
		builder.moveToElement(VendorEle).pause(2000);
				
		builder.click(searchVendorEle).perform();
		
		
	}

	@Then("Enter the Vendor Taxid as (.*)")
	public void enterTheVendorTaxid(String VID ) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("vendorTaxID").sendKeys(VID);
	}

	@Then("Click on Search button")
	public void clickOnSearchButton() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("buttonSearch").click();
	}


}
