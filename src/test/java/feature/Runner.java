package feature;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith (Cucumber.class)

@CucumberOptions(features="src/test/java/feature",
					glue="steps", 
					monochrome = true,
					//dryRun=true,
					snippets=SnippetType.CAMELCASE,plugin= {"pretty","html:report/basic"})

public class Runner {


}